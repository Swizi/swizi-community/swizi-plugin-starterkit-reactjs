# React-Redux Starter Kit for Swizi Plugin

This starter-kit is a project you can fork to start a new react plugin for Swizi.
It contains base elements for view rendering and asynchronous actions with redux-thunk.

this starter kit is built to work with our [Chrome extention](https://chrome.google.com/webstore/detail/swizi-plugin-dev-helper/kafcdkbaiijoccgiefljmlanpabnigao). It will emulate the behaviour of the swizi sdk when running on device. We recommend you to use it, but if you don't want, read [this part](#wait-for-gamoready-event) so you can run your plugin on computer.

## Table of Contents

* [Setup](#setup)
* [About the environnement variables](#About-the-environnement-variables)
* [Build your plugin](#build-your-plugin)
* [How to use starter kit](#how-to-use-starter-kit)
  * [Wait for swiziReady event](#wait-for-swiziready-event)
  * [Use Components](#Use-Components)
  * [Use Containers](#Use-Containers)
  * [Use actions](#Use-actions)
  * [Add actions](#Add-actions)
  * [Logging your plugin](#logging-your-plugin)

---

## Setup

1. Clone this project
2. Install depedencies
```
npm install
```
3. Run dev server
```
npm start
```

4. Test Url http://localhost:8008. You should see a web page with a form, allowing you to browse git repository.

## About the environnement variables

This starter kit provide some usefule variable that may help you handle your different environnement.

You can define them by adding `NODE_ENV=<variable_name>` in your script.

```
cross-env NODE_ENV=production webpack
```

The different variables are defined in `webpack.config.js` like so :

```javascript
new webpack.DefinePlugin({
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        __DEV__: JSON.stringify(process.env.NODE_ENV === 'development'),
        __PREPROD__: JSON.stringify(process.env.NODE_ENV === 'preproduction'),
        __PROD__: JSON.stringify(process.env.NODE_ENV === 'production'),
        __STATIC__: JSON.stringify(process.env.NODE_ENV === 'static'),
        __ENABLE_DEVTOOLS__: JSON.stringify(process.env.ENABLE_DEVTOOLS === 'true'),
}),
```

|    variable name     | available in code with  |                   description                    |
| :------------------: | :---------------------: | :----------------------------------------------: |
| process.env.NODE_ENV |        NODE_ENV         |       The name of the current env variable       |
|     development      |       \_\_DEV\_\_       |  A boolean true if NODE_ENV equal "development"  |
|    preproduction     |     \_\_PREPROD\_\_     | A boolean true if NODE_ENV equal "preproduction" |
|      production      |      \_\_PROD\_\_       |  A boolean true if NODE_ENV equal "production"   |
|        static        |     \_\_STATIC\_\_      |    A boolean true if NODE_ENV equal "static"     |
|   enable devtools    | \_\_ENABLE_DEVTOOLS\_\_ |  A boolean true if ENABLE_DEVTOOLS equal "true"  |

By defining global variables you will be able to define special use case in your code like so :

```javascript
export default {
        apiKey : __DEV__ ? 'developmentApiKey' : 
                __PREPROD__ ? 'preproductionApikey' : 
                __PROD__ ? 'productionApiKey' : '',
}
```

The `__STATIC__` variable is usefull when you need to make the plugin work without the player nor the chrome extension

```javascript
let something = null;
if (__STATIC__)
  // The plugin is neither running on device nor the chrome extension
  // so, we don't have access to swizi sdk
  // so, we provide a hardcoded value
  something = "onevalue";
else {
  // We have access to swizi or the extension
  // So we can retrieve a value from swizi sdk
  swizi.getItemForKey("keyOfTheValue").then(res => {
    something = res;
  });
}
```

The `__ENABLE_DEVTOOLS__` variable can be used for example, if you want to use the redux extension.

```javascript
let enhancer

if (__ENABLE_DEVTOOLS__) {
    enhancer = compose(
        applyMiddleware(ReduxThunk),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
} else {
    enhancer = applyMiddleware(ReduxThunk)
}

export default function configureStore(initialState) {
    return createStore(Reducer, initialState, enhancer)
}
```

## Build your plugin

To build your application, run one of the following scripts :

```
npm run build:static
npm run build:dev
npm run build:preprod
npm run build:prod
```

## How to use starter kit

### Wait for swiziReady event

`src/index.js`

```javascript
document.addEventListener('swiziReady', function () {
        ...
})
```

To be sure the swizi sdk is available when working with the [Chrome extention](https://chrome.google.com/webstore/detail/swizi-plugin-dev-helper/kafcdkbaiijoccgiefljmlanpabnigao), you have to wait for the `swiziReady` event to be fired. This event is fired when running your plugin on device and with the extension.

### Use Components

Components are located under `src/components`.

Their main purpose is to handle the view and nothing else. If you have logic to do, don't render the component directly. Instead, define and render a [container]() the will render the component and handle the logic for it.

### Use Containers

Containers are located under `src/containers`. This is where you should handle your logic. All actions are also binded here.

### Use actions

Starter kit is provided with one main files to register your actions and one file to write your contants.

Keep in mind that you are still able to add your own file, or rename the exisisting one as long as you configure the rest of the project to handle them.

All actions ar located in `src/redux/actions/actions.js`. For preference reasons, the reducer is also in this file

### Add actions

#### Define the constant that identify the action in `src/redux/constants/actionTypes.js`.

> You can also place it in a different file but remember to import it in the action creator file.

#### Define the action in the desired file

```javascript
function doAction() {
  return { type: actionTypes.DO_ACTION };
}
```

#### Don't forget to handle it in the reducer at the top of the file

```javascript
export function yourReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case actionTypes.DO_ACTION:
      return {
        // Do what you want with the initial state here
      };
    default:
      return state;
  }
}
```

if you use your own reducer don't forget to import it in the container you need and add it in `mapStateToProps`. Also, if you use your own action file you will have to bind his actions in `mapDispatchToProps`

```javascript
import * as otherActions from "../redux/actions/otherActionsFile";
import * as yourActions from "../redux/actions/yourActionsFile";

const mapDispatchToProps = dispatch => ({
  otherActions: bindActionCreators(otherActions, dispatch),
  yourActions: bindActionCreators(yourActions, dispatch),
});

// parameters are the name of the reducers defined in the action file
const mapStateToProps = ({ otherReducer, yourReducer }) => ({
  otherData: otherReducer,
  yourData: yourReducer,
});
```

All reducers are set up in the same store. So if you defined your own reducer, remember to add it in `src/redux/rootReducer.js`

```javascript
import { otherReducer } from "./actions/otherActionsFile";
import { yourReducer } from "./actions/yourActionsFile";

export default combineReducers({
  otherReducer,
  yourReducer,
});
```

### Logging your plugin

When it comes to log, don't use `console.log` as they will be displayed in Android and browser but not in the IOS log screen. Instead, use the `swizi.log()` function provided in `swizi.js`.

for more details about `swizi.log` see https://gitlab.com/swizi-community/Plugin#logs
