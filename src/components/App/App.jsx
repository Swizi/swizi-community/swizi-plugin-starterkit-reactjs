import React from 'react'
import PropTypes from 'prop-types'
import swizi from 'swizi'

import './App.less'

export default class App extends React.Component {
    state = {
        project: 'pong',
        language: 'java',
        device: '',
        version: '',
    }

    componentWillMount() {
        const self = this

        swizi.getPlatform().then(function(d) {
            swizi.getPlatformVersion().then(function(v) {
                let device = d
                let version = ''
                let temp = v

                if (temp.major) version = temp.major + '.' + temp.minor + '.' + temp.patchVersion
                else if (temp.sdk_int) version = temp.sdk_int + '.' + temp.security_patch + '.' + temp.release

                self.setState({
                    device: device,
                    version: version,
                })
            })
        })
    }

    changeName = e => {
        this.setState({
            project: e.target.value,
        })
    }

    changeLang = e => {
        swizi.log('log', 'Language changed for ' + e.target.value)
        this.setState({
            language: e.target.value,
        })
    }

    doSearch = e => {
        e.preventDefault()
        this.props.handleSearch(this.state.project, this.state.language)
    }

    render() {
        let tab = []

        if (this.props.actionsData.gitRepo.success) {
            tab = this.props.actionsData.gitRepo.data.items.map((res, key) => (
                <li key={key}>
                    <img src={res.owner.avatar_url} />
                    <div>
                        <p>{res.full_name}</p>
                        <small>{res.description}</small>
                    </div>
                </li>
            ))
        }

        return (
            <div>
                <nav>
                    <p>swizi Plugin Starter Kit</p>
                    <small>
                        {this.state.device} {this.state.version}
                    </small>
                </nav>
                <div>
                    <form>
                        <div>
                            <p>Nom de projet</p>
                            <input
                                type="text"
                                id="project"
                                name="project"
                                value={this.state.project}
                                onChange={this.changeName}
                            />
                        </div>

                        <div>
                            <p>Language</p>
                            <select
                                name="language"
                                id="language"
                                value={this.state.language}
                                onChange={this.changeLang}
                            >
                                <option value="assembly">assembly</option>
                                <option value="java">java</option>
                                <option value="c">c</option>
                                <option value="c++">c++</option>
                            </select>
                        </div>

                        <button onClick={this.doSearch}>Lancer une recherche</button>
                    </form>
                </div>
                <div className="content">
                    {!this.props.actionsData.gitRepo.loading && <ul>{tab}</ul>}

                    {this.props.actionsData.gitRepo.data &&
                        this.props.actionsData.gitRepo.data.total_count == 0 && (
                            <p className="noResult">Auncun résultat</p>
                        )}

                    {this.props.actionsData.gitRepo.loading && (
                        <div className="spinner">
                            <div className="double-bounce1" />
                            <div className="double-bounce2" />
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

App.propTypes = {
    actionsData: PropTypes.object.isRequired,
    handleSearch: PropTypes.func.isRequired,
}
