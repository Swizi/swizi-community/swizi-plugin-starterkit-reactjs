import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import './styles/styles.less'
import swizi from 'swizi'

import AppContainer from './containers/AppContainer'

import configureStore from './redux/store'
const store = configureStore()

// Waiting for swizi to be available
if (__ENABLE_DEVTOOLS__) {
    document.addEventListener('swiziReady', function() {
        swizi.log('log', 'App starting in ' + NODE_ENV + ' mode')

        render(
            <Provider store={store}>
                <AppContainer />
            </Provider>,
            document.getElementById('root')
        )
    })
} else {
    swizi.log('log', 'App starting in ' + NODE_ENV + ' mode')

    render(
        <Provider store={store}>
            <AppContainer />
        </Provider>,
        document.getElementById('root')
    )
}
