import * as actionTypes from '../constants/actionTypes'
import swizi from 'swizi'

/*
 * reducer
 */

const INITIAL_STATE = {
    gitRepo: { data: null, loading: false, success: false },
}

export function actionsReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
    case actionTypes.REPO_LOADING:
        return {
            ...state, gitRepo:
                { ...state.gitRepo, loading: true },
        }
    case actionTypes.REPO_LOADED:
        return {
            ...state, gitRepo:
                { ...state.gitRepo, loading: false, success: true, data: action.res },
        }
    default:
        return state
    }
}

let fetchOptions = {
    method: 'GET',
    headers: {
        Accept: 'application/json',
    },
}

/*
 * action creators
 */

export function fetchGitRepo(name, lang) {
    return (dispatch) => {
        dispatch(repoLoading())
        fetch('https://api.github.com/search/repositories?q=' + name + '+language:' + lang + '&sort=stars&order=desc', fetchOptions)
            .then(r => r.json())
            .then(function (response) {
                swizi.log('log', response)
                dispatch(repoLoaded(response))
            })
    }
}
function repoLoading() {
    return { type: actionTypes.REPO_LOADING }
}
function repoLoaded(res) {
    return { type: actionTypes.REPO_LOADED, res }
}