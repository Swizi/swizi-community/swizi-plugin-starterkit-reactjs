import { createStore, applyMiddleware } from 'redux'
import Reducer from './rootReducer'
import ReduxThunk from 'redux-thunk'

// Middleware you want to use in production:
const enhancer = applyMiddleware(ReduxThunk)

export default function configureStore(initialState) {
    return createStore(Reducer, initialState, enhancer)
}