import { combineReducers } from 'redux'

import { actionsReducer } from './actions/actions'

export default combineReducers({
    actionsReducer,
})