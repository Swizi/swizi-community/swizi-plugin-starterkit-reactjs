import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'

import * as Actions from '../redux/actions/actions'

import App from '../components/App/App'

class AppContainer extends React.Component {
    state = {}

    componentWillMount() {}

    handleSearch = (name, lang) => {
        this.props.actions.fetchGitRepo(name, lang)
    }

    render() {
        return <App actionsData={this.props.actionsData} handleSearch={this.handleSearch} />
    }
}

const mapStateToProps = ({ actionsReducer }) => ({
    actionsData: actionsReducer,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Actions, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)

AppContainer.propTypes = {
    actions: PropTypes.object.isRequired,
    actionsData: PropTypes.object.isRequired,
}
