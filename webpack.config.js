/* eslint-disable */
const webpack = require('webpack')
const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ZipPlugin = require('zip-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

// Create multiple instances
const extractCSS = new ExtractTextPlugin('[name]-css.css')
const extractLESS = new ExtractTextPlugin('[name]-less.css')

const config = {
    entry: {
        main: path.resolve(__dirname, 'src/index.js'),
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
    },
    devServer: {
        inline: true,
        port: 8008,
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    compact: false,
                    presets: ['env', 'react', 'stage-0'],
                    plugins: ['transform-class-properties'],
                },
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ['file-loader'],
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: ['file-loader'],
            },
            {
                test: /\.css$/,
                use: extractCSS.extract(['css-loader']),
            },
            {
                test: /\.less$/i,
                use: extractLESS.extract(['css-loader', 'less-loader']),
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            __DEV__: JSON.stringify(process.env.NODE_ENV === 'development'),
            __PREPROD__: JSON.stringify(process.env.NODE_ENV === 'preproduction'),
            __PROD__: JSON.stringify(process.env.NODE_ENV === 'production'),
            __STATIC__: JSON.stringify(process.env.NODE_ENV === 'static'),
            __ENABLE_DEVTOOLS__: JSON.stringify(process.env.ENABLE_DEVTOOLS === 'true'),
        }),
        extractCSS,
        extractLESS,
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'public/index.html'),
        }),
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, 'public'),
                to: path.resolve(__dirname, 'dist/'),
            },
        ], {
            ignore: ['index.html'],
        }),
        new ZipPlugin({
            filename: 'package-' + process.env.NODE_ENV + '-' + Date.now() + '.zip',
            path: 'zip/',
        }),
    ],
}

module.exports = config
